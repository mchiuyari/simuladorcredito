package com.javaheros.chiuyari.simuladorcredito;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;

public class CronogramaActivity extends AppCompatActivity {
    private TableLayout tableLayout;

    private double montoprestado;
    private double tasaanual;
    private int plazo;
    private double cuotamensual;
    private double saldocuota;
    private double interescuota;
    private double amortizacioncuota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cronograma);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setSubtitle("Cronograma de pagos");
        }

        Bundle extras = getIntent().getExtras();

        float a=1;
        float meses=12;

        montoprestado = Double.parseDouble(extras.getString("montoprestado"));
        tasaanual = Double.parseDouble(extras.getString("tasaanual"));
        Log.i("tasaanual: ",""+tasaanual);
        plazo = Integer.parseInt(extras.getString("plazo"));



        //double SD = 0.0;//seguro de desgravamen, no se usa por ahora
        double ITF = montoprestado*0.00005;//0.005%
        montoprestado = montoprestado + ITF;
        Log.i("montoprestado: ",""+montoprestado);
        //double TCEA = tasaanual + 0.01;
        //tasaanual = (double)(TCEA/meses);
        //float tasa = TCEA;

        /***todo lo trabajamos con float***/
        float b=360;

        float tasa1 = (float)(tasaanual/100);
        float TCEA = (float)Math.pow(1+tasa1,12)-1;
        float tasa = TCEA;

        float porcentaje=100;
        float mes = (float)30.42;
        float TED =  ((float)Math.pow(a + tasa, a / b))-a;
        TED = TED * porcentaje;
        //double TED = Math.pow(1 + tasaanual, 1 / 360)-1;
        Log.i("TED: ",""+TED);
        float Fi;//factor mensual
        float EFi = 0;//sumatoria del factor mensual
        float interes;
        for(int i=1; i<=plazo; i++){
            Fi = a/(float)Math.pow(a+(TED/porcentaje),mes*i);
            Log.i("Fi: ",""+Fi);
            EFi = EFi + Fi;
            Log.i("EFi: ",""+EFi);
        }

        float montop = (float) montoprestado;

        //float mes1 = 30;
        interes = montop*((float)Math.pow(a+(TED/porcentaje),mes)-a);

        float cuota = montop/EFi;//factor mensual

        //cuotamensual = ((tasaanual/100)*montoprestado)/(1-Math.pow(1+(tasaanual/100),-plazo));
        cuotamensual = cuota;
        //interescuota = ((tasaanual/100)*montoprestado);
        interescuota = interes;
        amortizacioncuota = (cuotamensual - interescuota);
        saldocuota = (montoprestado - amortizacioncuota);

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        llenartabla(TED);
    }

    public void llenartabla(float TED){
        Log.i("MenuActivity", "Iniciando el metodo llenarTabla");
        int hTituloContent = 40;
        TextView nrocuota;
        TextView cuota;
        TextView interes;
        TextView amortizacion;
        TextView saldo;
        //llenamos las cabeceras
        final TableRow tr_header = new TableRow(this);
        TableRow.LayoutParams lp_header = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        tr_header.setLayoutParams(lp_header);

        nrocuota = new TextView(this);
        nrocuota.setId(0);
        nrocuota.setText("Nro.".toString());
        nrocuota.setTextSize(15);
        nrocuota.setTextColor(Color.BLACK);
        nrocuota.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.15f));
        tr_header.addView(nrocuota);

        cuota = new TextView(this);
        cuota.setId(0);
        cuota.setText("cuota".toString());
        cuota.setTextSize(15);
        cuota.setTextColor(Color.BLACK);
        cuota.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
        tr_header.addView(cuota);

        interes = new TextView(this);
        interes.setId(0);
        interes.setText("interes".toString());
        interes.setTextSize(15);
        interes.setTextColor(Color.BLACK);
        interes.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
        tr_header.addView(interes);

        amortizacion = new TextView(this);
        amortizacion.setId(0);
        amortizacion.setText("amortizacion".toString());
        amortizacion.setTextSize(15);
        amortizacion.setTextColor(Color.BLACK);
        amortizacion.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
        tr_header.addView(amortizacion);

        saldo = new TextView(this);
        saldo.setId(0);
        saldo.setText("saldo".toString());
        saldo.setTextSize(15);
        saldo.setTextColor(Color.BLACK);
        saldo.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
        tr_header.addView(saldo);

        tableLayout.addView(tr_header,0);

        for(int i=0;i<plazo;i++){
            try {
                //final JugadorBean jugadorBean = arrayjugadores.get(i);
                final TableRow tr = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                tr.setLayoutParams(lp);

                nrocuota = new TextView(this);
                nrocuota.setId(i + 1);
                nrocuota.setText("" + (i + 1));
                nrocuota.setTextSize(12);
                nrocuota.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
                nrocuota.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.15f));
                tr.addView(nrocuota);

                cuota = new TextView(this);
                cuota.setId(i + 1);
                cuota.setText(String.format("%.2f", cuotamensual));
                cuota.setTextSize(12);
                cuota.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
                cuota.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
                tr.addView(cuota);

                interes = new TextView(this);
                interes.setId(i + 1);
                interes.setText(String.format("%.2f", interescuota));
                interes.setTextSize(12);
                interes.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
                interes.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
                tr.addView(interes);

                amortizacion = new TextView(this);
                amortizacion.setId(i + 1);
                amortizacion.setText(String.format("%.2f", amortizacioncuota));
                amortizacion.setTextSize(12);
                amortizacion.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
                amortizacion.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
                tr.addView(amortizacion);

                saldo = new TextView(this);
                saldo.setId(i + 1);
                saldo.setText(String.format("%.2f", saldocuota));
                saldo.setTextSize(12);
                saldo.setGravity(Gravity.LEFT | Gravity.CENTER_HORIZONTAL);
                saldo.setLayoutParams(new TableRow.LayoutParams(0, hTituloContent, 0.20f));
                tr.addView(saldo);

                tableLayout.addView(tr,i+1);

            }catch (Exception e){
                e.printStackTrace();
            }

            float a = 1;
            float mes = 30;
            float porcentaje = 100;
            float interes1 = ((float)saldocuota)*((float)Math.pow(a+(TED/porcentaje),mes)-a);
            interescuota = interes1;
            Log.i("Interes : ",""+interescuota);
            //interescuota = ((tasaanual/100)*saldocuota);
            amortizacioncuota = (cuotamensual - interescuota);
            saldocuota = (saldocuota - amortizacioncuota);
        }
        Log.i("MenuActivity", "fin del metodo llenarTabla");

    }
}
