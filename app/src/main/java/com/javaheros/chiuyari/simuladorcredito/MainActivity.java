package com.javaheros.chiuyari.simuladorcredito;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    EditText editImporte, editDuracion, editTipo, editSegFam, editNegPro, editFamPro, editProFi;
    CheckBox checkSegFam, checkNegPro, checkFamPro,checkProFi;
    Button botonCronograma;

    //double SD = 0.0;//seguro de desgravamen, no se usa por ahora
    //double ITF = montoprestado*0.00005;//0.005%
    //double TCEA = TEA + 0.01;
    //double TED = sqrt(1+TEA, 1/360)-1;
    //double Fi = 1/sqrt(1+TED,30);//factor mensual
    //double EFi = nuemrocuota*Fi//sumatoria del factor mensual

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setSubtitle("Datos de entrada");
        }

        editDuracion = (EditText) findViewById(R.id.editDuracion);
        editTipo = (EditText) findViewById(R.id.editInteres);
        editImporte = (EditText) findViewById(R.id.editImporte);
        botonCronograma = (Button) findViewById(R.id.buttonCronograma);

        editSegFam = (EditText) findViewById(R.id.editSegFamiliar);
        editSegFam.setEnabled(false);
        editNegPro = (EditText) findViewById(R.id.editNegPro);
        editNegPro.setEnabled(false);
        editFamPro = (EditText) findViewById(R.id.editFamPro);
        editFamPro.setEnabled(false);
        editProFi = (EditText) findViewById(R.id.editProFin);
        editProFi.setEnabled(false);

        checkSegFam = (CheckBox) findViewById(R.id.checkSegFam);
        checkNegPro = (CheckBox) findViewById(R.id.checkNegPro);
        checkFamPro = (CheckBox) findViewById(R.id.checkFamPro);
        checkProFi = (CheckBox) findViewById(R.id.checkProFin);

        botonCronograma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editImporte.getText().toString().equals("") || editDuracion.getText().toString().equals("") || editTipo.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "No deje campos en blanco", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, CronogramaActivity.class);
                    intent.putExtra("montoprestado", editImporte.getText().toString());
                    intent.putExtra("tasaanual", editTipo.getText().toString());
                    intent.putExtra("plazo", editDuracion.getText().toString());
                    startActivity(intent);
                }
            }
        });

        checkSegFam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(editDuracion.getText().toString().equals("")||editTipo.getText().toString().equals("")||editImporte.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Rellene el importe, el plazo y el interes",Toast.LENGTH_LONG).show();
                }else{
                    if (isChecked) {
                        editSegFam.setText("" + (Double.parseDouble(editDuracion.getText().toString()) * 4.5));
                        double total = Double.parseDouble(editImporte.getText().toString()) + (Double.parseDouble(editDuracion.getText().toString()) * 4.5);
                        editImporte.setText("" + total);
                    } else {
                        editSegFam.setText("0");
                        double total = Double.parseDouble(editImporte.getText().toString()) - (Double.parseDouble(editDuracion.getText().toString()) * 4.5);
                        editImporte.setText("" + total);
                    }
                }
            }
        });

        checkNegPro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (editDuracion.getText().toString().equals("") || editTipo.getText().toString().equals("") || editImporte.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Rellene el importe, el plazo y el interes", Toast.LENGTH_LONG).show();
                } else {
                    if (isChecked) {
                        editNegPro.setText("" + (Double.parseDouble(editDuracion.getText().toString()) * 4.34));
                        double total = Double.parseDouble(editImporte.getText().toString()) + (Double.parseDouble(editDuracion.getText().toString()) * 4.34);
                        editImporte.setText("" + total);
                    } else {
                        editNegPro.setText("0");
                        double total = Double.parseDouble(editImporte.getText().toString()) - (Double.parseDouble(editDuracion.getText().toString()) * 4.34);
                        editImporte.setText("" + total);
                    }
                }
            }
        });

        checkFamPro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (editDuracion.getText().toString().equals("") || editTipo.getText().toString().equals("") || editImporte.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Rellene el importe, el plazo y el interes", Toast.LENGTH_LONG).show();
                } else {
                    if (isChecked) {
                        editFamPro.setText("" + (Double.parseDouble(editDuracion.getText().toString()) * 3));
                        double total = Double.parseDouble(editImporte.getText().toString()) + (Double.parseDouble(editDuracion.getText().toString()) * 3);
                        editImporte.setText("" + total);
                    } else {
                        editFamPro.setText("0");
                        double total = Double.parseDouble(editImporte.getText().toString()) - (Double.parseDouble(editDuracion.getText().toString()) * 3);
                        editImporte.setText("" + total);
                    }
                }
            }
        });

        checkProFi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (editDuracion.getText().toString().equals("") || editTipo.getText().toString().equals("") || editImporte.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Rellene el importe, el plazo y el interes", Toast.LENGTH_LONG).show();
                } else {
                    if(isChecked){
                        editProFi.setText(""+(13+(Double.parseDouble(editDuracion.getText().toString())*0.03)));
                        double total = Double.parseDouble(editImporte.getText().toString())+(13+(Double.parseDouble(editDuracion.getText().toString())*0.03));
                        editImporte.setText("" + total);
                    }else{
                        editProFi.setText("0");
                        double total = Double.parseDouble(editImporte.getText().toString()) - (13+(Double.parseDouble(editDuracion.getText().toString())*0.03));
                        editImporte.setText(""+total);
                    }
                }
            }
        });

    }
}
